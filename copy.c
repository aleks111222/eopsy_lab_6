#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[]) {

   int option;
   
   if((option = getopt(argc, argv, "mh")) != -1) { // options -m or -h of none
      switch(option) {

         case 'm':         
            	
           	if (argc == 4) {
           		
           		int fr; // descriptor of a file to be copied
	   			fr = open(argv[2], O_RDONLY, 0600);
	   				
   				if (fr == -1) {
					perror("failed to open file 1\n");
					return 0;
   				}
   			
   				struct stat fs; // used for getting number of bytes of the file
				stat(argv[2], &fs);
				
           		char* mem1 = mmap(0, fs.st_size, PROT_READ, MAP_SHARED, fr, 0); // PROT_READ because file is opened in read only mode; assign memory space for all of the content
           		
 				if (mem1 == MAP_FAILED)
  					perror("file 1 mmap error\n");
  					
  				int fw; // file copying from the first one
  				fw = open(argv[3], O_RDWR, 0600);

  				ftruncate(fw, fs.st_size); // change size of the file according to the size of the first file
  				
  				if (fw == -1) {
  					perror("failed to open file 2\n");
  					return 0;
  				}
  					
 				char* mem2 = mmap(0, fs.st_size, PROT_WRITE|PROT_READ, MAP_SHARED, fw, 0);
 				if (mem2 == MAP_FAILED)
  					perror("file 2 mmap error\n");

 				close (fr); // close descriptors
 				close (fw);

 				memcpy(mem2, mem1, fs.st_size); // copies bytes from mem1 to mem2

 				munmap(mem1, fs.st_size); // remove the pages mappings
				munmap(mem2, fs.st_size);

           	}
           	else {
           		perror("wrong syntax, check the help option.\n"); // files not specified
           	}
           		
            break;
            	
         case 'h':
         
            	printf("Program for copying content of a one file to another.\nUsage:\n copy -m [filename1] [filename2]\n  The content of the first file will be copied into the second file.\n");
            	printf("  (This option uses memcpy function)\n");
            	printf(" copy [filename1] [filename2]\n");
            	printf("  Outcome identical as for the previous option.\n");
            	printf("  (This option uses write function)\n");
            	printf(" copy -h\n");
            	printf("  This manual will be shown.\n");
            	break;
      }
   }
   else {
   		if (argc == 3) {
   		
   			int fr;
   			fr = open(argv[1], O_RDONLY, 0600);
   			if (fr == -1) {
				perror("failed to open file 1");
				return 0;
   			}
   			
   			struct stat fs;
			stat(argv[1], &fs);

			char buf[fs.st_size]; // buffer for reading and writing characters
			bzero(buf, fs.st_size);

			read(fr, buf, fs.st_size); // reading from the file into the buffer

			close(fr);

			int fw;
   			fw = open(argv[2], O_RDWR, 0600);
   			if (fw == -1) {
				perror("failed to open file 2");
				return 0;
   			}

   			write(fw, buf, fs.st_size); // writing from the buffer into the file

			close(fw);
   		}
   }
}
